<?php


class Drink extends Item
{
    protected $discount;

    public function __construct($title, $price, $discount)
    {
        parent::__construct($title, $price);
        $this->discount = $discount;
    }

    public static function getType()
    {
        return parent::getType() . 'drink';
    }

    public function getPrice()
    {
        return $this->price - $this->discount;
    }

    public function getSummary()
    {
        return 'Title: ' . $this->title . ' Price: ' . $this->price . ' Type: ' . parent::getType(); //used parent to get property from parent class
    }
}