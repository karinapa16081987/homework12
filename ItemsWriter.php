<?php


class ItemsWriter
{
    protected $items = [];

    public function addItem(Writable $items)
    {
        $this->items[] = $items;
    }

    public function write()
    {
        $result = '';
        foreach ($this->items as $item) {
            $result .= $item->getSummary();
        }
        return $result;
    }
}