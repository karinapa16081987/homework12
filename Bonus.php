<?php


class Bonus implements Writable
{
    protected $title;
    protected $type;
    protected $description;

    public function getSummary()
    {
        return 'Title: ' . $this->title . ' Type: ' . $this->type . ' Description: ' . $this->description;
    }
}