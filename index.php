<?php

require_once('Writable.php');
require_once('Item.php');
require_once('Food.php');
require_once('Drink.php');
require_once('Bonus.php');
require_once('ItemsWriter.php');

$drink = new Drink('Beer', 12, 10);
$food = new Food('Chocolate', 25);
$item = new ItemsWriter();
$item->addItem($food);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h3>Test Page</h3>
</div>
<div class="container">
    <ul>
        <li>
            <?= $drink->getSummary() ?>
        </li>
        <li>
            <?= $food->getSummary() ?>
        </li>
        <li>
            <?= $item->write() ?>
        </li>
    </ul>
</div>
</body>
</html>
