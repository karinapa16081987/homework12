<?php


class Food extends Item
{
    public function __construct($title, $price)
    {
        parent::__construct($title, $price);
    }

    public static function getType()
    {
        return parent::getType() . ' food';
    }
    public function getPrice()
    {
        return $this->price*0.9;
    }
    public function getSummary()
    {
        return 'Title: '. $this->title . ' Price: ' . $this->price . ' Type: '. self::getType(); //used self here to get property from this class
    }
}